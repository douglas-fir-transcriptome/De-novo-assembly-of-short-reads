#!/bin/bash
#SBATCH --job-name=1165Trimmomatic1
#SBATCH -n 24
#SBATCH -N 1
#SBATCH -c 24
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

OUTPATH="/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/3_Quality_control/3.2_Quality_trim/Trimmomatic"
cd /labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/1_Raw_reads/

echo hostname
date

module load Trimmomatic/0.36

for fname in *R1*; do
  base=$(basename $fname R1.fastq.gz)
  rname=${base}R2.fastq.gz

echo ${base}

java -jar $Trimmomatic \
  PE -threads 24 \
  -phred33 \
  -trimlog ${OUTPATH}/logfile \
  $fname \
  $rname \
  $OUTPATH/trim_${base}_R1_paired.fastq.gz \
  $OUTPATH/trim_${base}_R1_unpaired.fastq.gz \
  $OUTPATH/trim_${base}_R2_paired.fastq.gz \
  $OUTPATH/trim_${base}_R2_unpaired.fastq.gz \
  ILLUMINACLIP:NEBNext_dual_adaptor.fasta:2:30:10:2:keepBothReads \
  LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:30

done

date
~
