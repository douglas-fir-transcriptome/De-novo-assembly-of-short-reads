# Douglas-fir-transcriptome analysis using short reads  

[Step 0 What is the experiment about](/#step-0-about-the-experiment)  
[Step 1 Clip adaptors and quality trimming using Trimmomatic](https://gitlab.com/douglas-fir-transcriptome/Douglas-fir-short-read-transcriptome#step-1-clip-adaptors-and-quality-trimming-using-trimmomatic)  
[Step 2 Assess quality using FastQC and MultiQC](https://gitlab.com/douglas-fir-transcriptome/Douglas-fir-short-read-transcriptome#step-2-assess-quality-using-fastqc-and-multiqc)  
[Step 3 De novo assembly using Trinity](https://gitlab.com/douglas-fir-transcriptome/Douglas-fir-short-read-transcriptome#step-3-de-novo-assembly-using-trinity)  
[Step 4 Identify coding regions using transdecoder](https://gitlab.com/douglas-fir-transcriptome/Douglas-fir-short-read-transcriptome#step-4-identify-coding-regions-using-transdecoder)  
[Step 5 Assess quality of de novo transcriptome assembly using RNAQuast](https://gitlab.com/douglas-fir-transcriptome/Douglas-fir-short-read-transcriptome#step-5-assess-quality-of-de-novo-transcriptome-assembly-using-busco-in-rnaquast-pipeline)  

## About the experiment  
 Summer experiment has four treatments and three tissues were collected   
 Fall experiment has five treatments (time and temperature) and only needles were collected  
 Four contrasting provenances of Douglas-fir were studied  
 
 We are using these short-read transcriptome for hybrid sequencing.  Here is my [workflow](https://drive.google.com/open?id=1RN_6JktYG0ozZmovGTyBdkbGEnZ5UFZ5).
 
## Step 1 Clip adaptors and quality trimming using Trimmomatic  

**INPUT**  
|--filename.R1.fastq.gz (or filename.R1.fastq)   
|--filename.R2.fastq.gz (or filename.R2.fastq)  
|--[NEBNext_dual_adaptors.fasta](/NEBNext_dual_adaptors.fasta)  

First, I downloaded my raw read data from the sequencing center.  I also downloaded the md5sum they generated.  I created an md5sum of my data and check the integrity of your download.  Use this command to do this:

```
md5sum filename.R1.fastq.gz >> readSet_download.md5
```

This is the NEBNext_dual_adaptors.fasta  
```
>PrefixPE1/1
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
>PrefixPE2/2
AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT
>PE1
AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC
>PE2
AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT
>PE1_rc
GTGACTGGAGTTCAGACGTGTGCTCTTCCGATCT
>PE2_rc
ACACTCTTTCCCTACACGACGCTCTTCCGATCT
```

**Script** [Trimmomatic1.sh](/Trimmomatic1.sh)

```
module load Trimmomatic/0.36

java -jar $Trimmomatic \
  PE -threads 24 \
  -phred33 \
  -trimlog logfile \
  filename.R1.fastq.gz \
  filename.R2.fastq.gz \
  trim_filename_R1_paired.fastq.gz \
  trim_filename_R1_unpaired.fastq.gz \
  trim_filename_R2_paired.fastq.gz \
  trim_filename_R2_unpaired.fastq.gz \
  ILLUMINACLIP:NEBNext_dual_adaptor.fasta:2:30:10:2:keepBothReads \
  LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:30
```
**OUTPUT**  
|--trim_filename_R1_paired.fastq.gz  
|--trim_filename_R1_unpaired.fastq.gz  
|--trim_filename_R2_paired.fastq.gz  
|--trim_filename_R2_unpaired.fastq.gz  

## Step 2 Assess quality using [FastQC](https://github.com/s-andrews/FastQC) and [multiQC](https://github.com/ewels/MultiQC)   

**INPUT**  
_Raw reads_   
|--filename.R1.fastq.gz (or filename.R1.fastq)  
|--filename.R2.fastq.gz (or filename.R2.fastq)  

_Quality reads_   
|--trim_filename_R1_paired.fastq.gz   
|--trim_filename_R2_paired.fastq.gz  

**Scripts** [Fastqc_rawreads.sh](/Fastqc_rawreads.sh) and [Fastqc_qualityreads.sh](/Fastqc_qualityreads.sh)  
```
module load fastqc/0.11.7

  fastqc -t 10 *.fastq.gz
  mkdir FastQC_trimmed
    mv trim* FastQC_trimmed
  mkdir FastQC_singles
    mv singles* FastQC_singles
 
module load MultiQC/1.7

 multiqc FastQC_trimmed
```

**OUTPUT**   
|--multiqc_report.html  
|--multiqc_data/  
|--- mqc_fastqc_adapter_content_plot_1.txt             
|--- mqc_fastqc_per_sequence_gc_content_plot_Counts.txt        
|--- mqc_fastqc_sequence_duplication_levels_plot_1.txt   
|--- multiqc_general_stats.txt  
|--- mqc_fastqc_overrepresented_sequencesi_plot_1.txt  
|--- mqc_fastqc_per_sequence_gc_content_plot_Percentages.txt  
|--- mqc_fastqc_sequence_length_distribution_plot_1.txt  multiqc.log  
|--- mqc_fastqc_per_base_n_content_plot_1.txt  
|--- mqc_fastqc_per_sequence_quality_scores_plot_1.txt  
|--- multiqc_data.json  
|--- multiqc_sources.txt  
|--- mqc_fastqc_per_base_sequence_quality_plot_1.txt   
|--- mqc_fastqc_sequence_counts_plot_1.txt   
|--- multiqc_fastqc.txt    

## Step 3 De novo assembly using [Trinity](https://github.com/trinityrnaseq/RNASeq_Trinity_Tuxedo_Workshop/wiki/Trinity-De-novo-Transcriptome-Assembly-Workshop)  

**INPUT**  
|--trim_filename_R1_paired.fastq.gz    
|--trim_filename_R2_paired.fastq.gz   

**Script** [Trinity.sh](/Trinity.sh)  

```
module load trinity/2.6.6

Trinity --seqType fq \
        --left trim_filename_R1_paired.fastq.gz  \
        --right trim_filename_R2_paired.fastq.gz  \
        --min_contig_length 350 \
        --CPU 16 \
        --max_memory 128G \
        --output $ASS/trinity_filename \
        --full_cleanup
```
**OUTPUT**  
|--trinity_trim_filename.Trinity.fasta  
|--trinity_trim_filename.Trinity.fasta.gene_trans_map  

While running or when run is incomplete you will also see this folder:  
|--trinity_trim_filename/  

## Step 4 Identify coding regions using [transdecoder](https://github.com/TransDecoder/TransDecoder/wiki)  
### For the interest of time, a subset of samples will be used to establish a workflow.  The list of samples are [***here***](/novaseq_subset.txt).    

**INPUT**  
|--trinity_trim_filename.Trinity.fasta  
|--Pfam-A.hmm which can be found in /isg/shared/databases/Pfam in Xanadu  

**Script** [transdecoder.sh](/transdecoder.sh)  
```
module load hmmer/3.2.1
module load TransDecoder/5.3.0

TransDecoder.LongOrfs -t trinity_trim_filename.Trinity.fasta

hmmscan --cpu 16 \
      --domtblout trinity_trim_filename.Trinity.fasta.pfam.domtblout \
      /isg/shared/databases/Pfam/Pfam-A.hmm \
     trinity_trim_filename.Trinity.fasta.transdecoder_dir/longest_orfs.pep

TransDecoder.Predict -t trinity_trim_filename.Trinity.fasta \
        --retain_pfam_hits trinity_trim_filename.Trinity.fasta.pfam.domtblout \
        --cpu 16
```

## Step 5 Assess quality of de novo transcriptome assembly using [BUSCO](https://busco.ezlab.org/) in [rnaQUAST](https://github.com/ablab/rnaquast) pipeline  

**INPUT**  
**Script**  
```
python rnaQUAST.py \
--transcripts /PATH/TO/transcripts1.fasta /PATH/TO/ANOTHER/transcripts2.fasta [...] \
--busco_lineage path/to/busco/lineage/data
```
**OUTPUT** 



