#!/bin/bash
#SBATCH --job-name=Trinity
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

hostname
date

module load trinity/2.6.6

TRI="/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/3_Quality_control/3.2_Quality_trim/Trimmomatic"
ASS="/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/5_Assembly"

cd $TRI

for fname in trim*R1_paired.fastq.gz; do

  base=$(basename $fname R1_paired.fastq.gz)
  rname=${base}R2_paired.fastq.gz

        Trinity --seqType fq \
        --left $fname \
        --right $rname \
        --min_contig_length 350 \
        --CPU 16 \
        --max_memory 128G \
        --output $ASS/trinity_${base} \
        --full_cleanup

done
