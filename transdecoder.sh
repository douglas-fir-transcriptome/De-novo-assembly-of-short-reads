#!/bin/bash
#SBATCH --job-name=transdecoder_subset
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --mem=100G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

ASS="/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/5_Assembly"
COD="/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/6_Coding_regions"

module load hmmer/3.2.1
module load TransDecoder/5.3.0

cd $COD

#Create symbolic link
for fname in `cat novaseq_subset.txt`; do
   ln -s ${ASS}/$fname
done

#renaming symbolic link
for fname in *Trinity.fasta; do
   mv $fname $(echo $fname | sed 's/NEBN.*_[A-Z][0-9][0-9]*\.GC/GC/');
done

#Running transdecoder
for fname1 in $COD/*.Trinity.fasta; do

   base1=$(basename $fname1 Trinity.fasta)

 mkdir $base1
 cd $base1

   TransDecoder.LongOrfs -t ${fname1}

   hmmscan --cpu 16 \
      --domtblout pfam.domtblout \
      /isg/shared/databases/Pfam/Pfam-A.hmm \
     ${fname1}.transdecoder_dir/longest_orfs.pep

   TransDecoder.Predict -t ${fname1} \
        --retain_pfam_hits pfam.domtblout \
        --cpu 16

 cd ..

done


rm  *Trinity.fasta

date

        