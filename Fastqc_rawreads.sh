#!/bin/bash
#SBATCH --job-name=FastQC
#SBATCH -n 10
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo hostname
date

cd /labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/1_Raw_reads/

module load fastqc/0.11.7

fastqc -t 10 *.fastq.gz

mv *fastqc* ../2_FastQC

cd /labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/2_FastQC

mkdir FastQC_trimmed
        mv trim* FastQC_trimmed
mkdir FastQC_singles
        mv singles* FastQC_singles

module load MultiQC/1.7

multiqc FastQC_trimmed


date
