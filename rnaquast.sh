#!/bin/bash
#SBATCH --job-name=rnaquast
#SBATCH -n 10
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=20G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo hostname
date

module load rnaquast
module load gmap
module load blastn

python rnaQUAST.py \

--transcripts /PATH/TO/transcripts1.fasta /PATH/TO/ANOTHER/transcripts2.fasta [...] \

--reference /PATH/TO/reference.fasta --gtf /PATH/TO/gene_coordinates.gtf
