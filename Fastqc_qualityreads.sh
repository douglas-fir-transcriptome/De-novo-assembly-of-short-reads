#!/bin/bash
#SBATCH --job-name=FastQC
#SBATCH -n 10
#SBATCH -N 1
#SBATCH -c 1
#SBATCH --mem=10G
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mail-user=vera.velasco@utoronto.ca
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

echo hostname
date

TRI="/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/3_Quality_control/3.2_Quality_trim/Trimmomatic"
FAS="/labs/Wegrzyn/CoAdapTree_Douglasfir/NovaSeq/4_FastQC"

cd $TRI

module load fastqc/0.11.7

 fastqc -t 10 *.fastq.gz
 
mkdir $FAS/FastQC_trimmed
mkdir $FAS/FastQC_singles

mv trim*fastqc* $FAS/FastQC_trimmed
mv singles*fastqc* $FAS/FastQC_singles

cd $FAS

module load MultiQC/1.7

 multiqc FastQC_trimmed
